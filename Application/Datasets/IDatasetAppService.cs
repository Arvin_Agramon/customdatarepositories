﻿using Abp.Application.Services;
using BonneyGroup.SSMS.Datasets.Dto;
using BonneyGroup.SSMS.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BonneyGroup.SSMS.Datasets
{
    public interface IDatasetAppService : IApplicationService
    {
        Task<PagedSortedAndSearchedResultDto<JobDto>> GetAllJobs();

        Task<bool> ExecuteRefreshDataset(string jobName);
    }
}
