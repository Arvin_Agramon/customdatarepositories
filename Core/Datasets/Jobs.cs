﻿using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations;

namespace BonneyGroup.SSMS.Datasets
{
    public class Jobs : AuditedEntity<Guid>
    {

        public string Name { get; set; }
    }
}
