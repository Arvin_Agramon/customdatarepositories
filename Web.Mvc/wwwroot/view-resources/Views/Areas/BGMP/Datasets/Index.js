﻿(function () {
    $(function () {
        var datasetVM = function (data) {
            var self = this;
            self.id = ko.observable(data.id);
            self.name = ko.observable(data.name);
        };

        var datasetTable = $('#datasetGrid');
        var datasetService = abp.services.app.dataset;

        var grid = datasetTable.dataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "/bgmp/Datasets/GetJobs",
                type: "POST"
            },
            columns: [
                { data: "id", orderable: true, searchable: true, name: "Id" },
                { data: "name", orderable: true, searchable: true, name: "Name" },
                { data: null, defaultContent: "<button type='button' class='btn btnRefreshData'><i class='material-icons'>edit</i></button>", targets: -1 }
            ],
            order: [[0, "asc"]]
        });

        $(document).on("click", '.btnRefreshData', function (e) {
            e.preventDefault();
            var that = this;

            var grid = datasetTable.DataTable();
            var data = grid.row(this).data();
            if (!data) {
                data = grid.row($(this).parents('tr')).data();
                RefreshSet(data.name);
            }

        });

        function RefreshSet(name) {
            abp.ui.setBusy();
            var result = datasetService.ExecuteRefreshDataset(name);

            if (result) {
                datasetTable.DataTable().ajax.reload();
            }
            abp.ui.clearBusy();

        }

    });
})();