﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;
using System.Data.SqlClient;
using System.Threading;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Data;
using Abp.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using BonneyGroup.SSMS.EntityFrameworkCore;
using BonneyGroup.SSMS.EntityFrameworkCore.Repositories;
using System.Data;

namespace BonneyGroup.SSMS.Datasets
{
    public class DatasetRepository : AbpZeroTemplateRepositoryBase<Jobs, Guid>, IDatasetRepository
    {
        private readonly IActiveTransactionProvider _transactionProvider;

        public DatasetRepository(IDbContextProvider<SSMSDbContext> dbContextProvider, IActiveTransactionProvider transactionProvider)
            : base(dbContextProvider)
        {
            _transactionProvider = transactionProvider;
        }

        public async Task<List<Jobs>> GetAllJobs()
        {
            EnsureConnectionOpen();

            using (var command = CreateCommand("SELECT job_id, [name] FROM msdb.dbo.sysjobs;", CommandType.Text))
            {
                using (var dataReader = await command.ExecuteReaderAsync())
                {
                    var result = new List<Jobs>();

                    while (dataReader.Read())
                    {
                        result.Add(new Jobs {
                            Id = (Guid)dataReader["job_id"],
                            Name = dataReader["name"].ToString()
                        });
                    }
                    return result;
                }
            }
        }

        public async Task<bool> ExecuteJobImmediate(string job_id)
        {
            EnsureConnectionOpen();

            using (var command = CreateCommand("EXEC dbo.sp_start_job N'" + job_id + "';", CommandType.Text))
            {
                int result = await command.ExecuteNonQueryAsync();

                if (result == 0)
                    return true;
                else
                    return false;
            }
        }

        private DbCommand CreateCommand(string commandText, CommandType commandType, params SqlParameter[] parameters)
        {
            var command = Context.Database.GetDbConnection().CreateCommand();

            command.CommandText = commandText;
            command.CommandType = commandType;
            command.Transaction = GetActiveTransaction();

            foreach (var parameter in parameters)
            {
                command.Parameters.Add(parameter);
            }

            return command;
        }

        private void EnsureConnectionOpen()
        {
            var connection = Context.Database.GetDbConnection();

            if (connection.State != ConnectionState.Open)
            {
                connection.Open();
            }
        }

        private DbTransaction GetActiveTransaction()
        {
            return (DbTransaction)_transactionProvider.GetActiveTransaction(new ActiveTransactionProviderArgs
            {
                {"ContextType", typeof(SSMSDbContext) },
                {"MultiTenancySide", MultiTenancySide }
            });
        }
    }
}
