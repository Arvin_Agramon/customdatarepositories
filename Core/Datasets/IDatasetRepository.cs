﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;

namespace BonneyGroup.SSMS.Datasets
{
    public interface IDatasetRepository : IRepository<Jobs, Guid>
    {

        Task<List<Jobs>> GetAllJobs();

        Task<bool> ExecuteJobImmediate(string job_id);
    }
}
