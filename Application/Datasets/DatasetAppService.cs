﻿using Abp.Domain.Repositories;
using BonneyGroup.SSMS.Datasets.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using System.Linq;
using Abp.Authorization;
using System;
using System.Linq.Expressions;
using BonneyGroup.SSMS.Models;

namespace BonneyGroup.SSMS.Datasets
{
    [AbpAuthorize]
    public class DatasetAppService : SSMSAppServiceBase, IDatasetAppService
    {
        private readonly IDatasetRepository _datasetRepository;

        public DatasetAppService(IDatasetRepository datasetRepository)
        {
            _datasetRepository = datasetRepository;
        }

        public async Task<PagedSortedAndSearchedResultDto<JobDto>> GetAllJobs()
        {
            List<Jobs> resultSet = await _datasetRepository.GetAllJobs();
            return ObjectMapper.Map<PagedSortedAndSearchedResultDto<JobDto>>(resultSet);
        }

        public async Task<bool> ExecuteRefreshDataset(string jobName)
        {
            bool ret = await _datasetRepository.ExecuteJobImmediate(jobName);

            return ret;
        }

    }
}
