﻿using System;

namespace BonneyGroup.SSMS.Datasets.Dto
{
    public class JobDto
    {
        public string Name { get; set; }
    }
}
