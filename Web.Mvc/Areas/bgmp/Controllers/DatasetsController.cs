﻿using Abp.AspNetCore.Mvc.Authorization;
using BonneyGroup.SSMS.Authorization;
using BonneyGroup.SSMS.Controllers;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using BonneyGroup.SSMS.Datasets;
using BonneyGroup.SSMS.Roles;
using DataTables.AspNet.Core;
using BonneyGroup.SSMS.Web.Models;
using DataTables.AspNet.AspNetCore;

namespace BonneyGroup.SSMS.Web.BGMP.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_Admin)]
    [Area("bgmp")]
    public class DatasetsController : SSMSControllerBase
    {
        private readonly IRoleAppService _roleAppService;
        private readonly IDatasetAppService _datasetAppService;

        public DatasetsController(IRoleAppService roleAppService, IDatasetAppService datasetAppService)
        {
            _roleAppService = roleAppService;
            _datasetAppService = datasetAppService;
        }

        public async Task<ActionResult> Index()
        {
            
            return View();
        }

        public async Task<IActionResult> GetJobs(IDataTablesRequest request)
        {
            var jobs = await _datasetAppService.GetAllJobs();
            var response = DataTablesResponse.Create(request, jobs.TotalCount, jobs.TotalFilteredCount, jobs.Items);
            return new DataTablesJsonResult(response, true);
        }
    }
}
